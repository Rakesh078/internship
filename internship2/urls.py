from django.conf.urls import include, url
from django.contrib import admin
from web.views import StudentInfo

urlpatterns = [
    # Examples:
    # url(r'^$', 'internship2.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^stud/', StudentInfo.as_view()),
    #url(r'^insert/', StudentInfo.as_view(),name='post'),
    #url(r'^del/', StudentInfo.as_view(),name='delete'),
    url(r'^update/(\d+)/$', StudentInfo.as_view(),name='put'),

]
