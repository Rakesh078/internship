#from _future_ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import QueryDict
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.

from django.http import HttpResponse
from .models import StudentDb


class StudentInfo(View):

   # methods to show record of student databases , using url it print all records on browser

    def get(self , request):
        result = []
        student = StudentDb.objects.all()
        for student_info in student:
            dict_res = {}
            dict_res['first_name'] = student_info.first_name
            dict_res['middle_name'] = student_info.middle_name
            dict_res['last_name']   = student_info.last_name
            dict_res['mobile_number'] = student_info.mobile_num
            dict_res['admission_date'] = student_info.admission_date
            dict_res['college_id'] =  student_info.college_id
            result.append(dict_res)

        return HttpResponse(result, status=400)

    #process to add reords in our studentDb modles

    def post(self , request):
        body = request.POST
        print "hellow"
        print body
        fname = body['f']
        mname = body['m']
        lname = body['l']
        mbnum = body['p']
        adate = body['d']
        cid   = body['c']

        obj = StudentDb(first_name = fname, middle_name = mname , last_name = lname , mobile_num = mbnum , admission_date = adate , college_id = cid)
        obj.save()
        print obj
        return HttpResponse('Items Updated')

    #methods to delete records using college_id from student database

    def delete(self , request):
        abc = QueryDict(request.body)
        print abc
        #print abc
        id  = abc['college_id']
        print id
        obj = StudentDb.objects.get(college_id = id)
        obj.delete()
        return HttpResponse("deleted hiiiiii")

    #method of updating records on data base on the process of given some id from urls


    def put(self, request , id):
       try:
           update_req=QueryDict(request.body)
           data=StudentDb.objects.get(id=id)
           if "first_name" in update_req.keys():
               data.first_name=update_req["first_name"]
           if "middle_name" in update_req.keys():
               data.middle_name=update_req["middle_name"]
           if "last_name" in update_req.keys():
               data.last_name=update_req["last_name"]
           if "mobile_num" in update_req.keys():
               data.mobile_num=update_req["mobile_num"]
           if "admission_date" in update_req.keys():
               data.admission_date=update_req["admission_date"]
           if "college_id" in update_req.keys():
               data.college_id=update_req["college_id"]

           data.save()
           return HttpResponse("updated",status=200)
       except ObjectDoesNotExist:
           return HttpResponse("obj not exit",status=500)
