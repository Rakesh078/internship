from django.db import models

# Create your models here.
class StudentDb(models.Model):
    first_name      = models.CharField(max_length=30)
    middle_name     = models.CharField(max_length=30)
    last_name       = models.CharField(max_length=30)
    mobile_num      = models.IntegerField()
    admission_date  = models.DateField()
    college_id      = models.IntegerField()

