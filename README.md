
Installation:

1. clone the repository: https://bitbucket.org/Rakesh078/internship.git
2. cd to cloned folder
3. install Pyenv
4. install pyenv-virtualenv
5. install python version 2.7
6. install django version 1.8


How to run:

1. type url as http://127.0.0.1:8000/stud  it will display the contents of StudentDb databases on browser.

2. Go to post man and copy the urls  http://127.0.0.1:8000/stud and to excess the Post and Delete methods data are entered using postman and updation and deletion will be done .

3. The put methods is executed using postman or browser in which following urls is enterd  http://127.0.0.1:8000/update/1/  this 1 is acts as id and values are updated according to it from urls in database.
